from dataset import Generator
import torch
from torch import nn, optim
from functools import reduce
from operator import mul
import os
from utils.bottleneck import Bottleneck, gn
import argparse
from utils.evaluation import accuracy, AverageMeter
import time

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
# Dataset setting

parser.add_argument('--dataset-path', type=str,
                    help='path to images')
parser.add_argument('--annot-path', type=str,
                    help='path for labels annotations of keypoints')

args = parser.parse_args()
generator = Generator(args.dataset_path, args.annot_path)

train_dataset, val_dataset = generator()

batch_size = 32


class FCN(nn.Module):
    '''Hourglass model from Newell et al ECCV 2016'''
    def __init__(self, block=Bottleneck):
        super(FCN, self).__init__()

        self.inplanes = 64
        self.num_feats = 128
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=True)
        self.bn1 = nn.GroupNorm(gn, self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.layer1 = self._make_residual(block, self.inplanes, 1)
        self.layer2 = self._make_residual(block, self.inplanes, 1)
        self.layer3 = self._make_residual(block, self.num_feats, 1)
        self.maxpool = nn.MaxPool2d(2, stride=2)

    def _make_residual(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=True),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def _make_fc(self, inplanes, outplanes):
        bn = nn.GroupNorm(gn, inplanes)
        conv = nn.Conv2d(inplanes, outplanes, kernel_size=1, bias=True)
        return nn.Sequential(
                conv,
                bn,
                self.relu,
            )

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.layer1(x)
        x = self.maxpool(x)
        x = self.layer2(x)
        x = self.layer3(x)

        return x


def linear_expectation(probs, values):
    assert (len(values) == probs.ndimension() - 2)
    expectation = []
    for i in range(2, probs.ndimension()):
        # Marginalise probabilities
        marg = probs
        for j in range(probs.ndimension() - 1, 1, -1):
            if i != j:
                marg = marg.sum(j, keepdim=False)
        # Calculate expectation along axis `i`
        expectation.append((marg * values[len(expectation)]).sum(-1, keepdim=False))
    return torch.stack(expectation, -1)


def flat_softmax(inp):
    """Compute the softmax with all but the first two tensor dimensions combined."""

    orig_size = inp.size()
    flat = inp.view(-1, reduce(mul, orig_size[2:]))
    flat = torch.nn.functional.softmax(flat, -1)
    return flat.view(*orig_size)


def normalized_linspace(length, dtype=None, device=None):
    """Generate a vector with values ranging from -1 to 1.

    Note that the values correspond to the "centre" of each cell, so
    -1 and 1 are always conceptually outside the bounds of the vector.
    For example, if length = 4, the following vector is generated:

    ```text
     [ -0.75, -0.25,  0.25,  0.75 ]
     ^              ^             ^
    -1              0             1
    ```

    Args:
        length: The length of the vector

    Returns:
        The generated vector
    """
    if isinstance(length, torch.Tensor):
        length = length.to(device, dtype)
    first = -(length - 1.0) / length
    return torch.arange(length, dtype=dtype, device=device) * (2.0 / length) + first


def frob(heatmaps):
    batch_count, _, height, width = heatmaps.size()

    dsnt_x = ((2. * torch.arange(1, width + 1) - (width + 1)) / width).repeat(batch_count, height, 1).to(
        device=heatmaps.device)
    dsnt_y = ((2. * torch.arange(1, height + 1) - (height + 1)) / height).repeat(batch_count, width, 1).permute(
        (0, 2, 1)).to(device=heatmaps.device)

    dsnt_x = dsnt_x.unsqueeze(dim=1)
    dsnt_y = dsnt_y.unsqueeze(dim=1)

    outputs_x = torch.sum(heatmaps * dsnt_x, dim=[2, 3])
    outputs_y = torch.sum(heatmaps * dsnt_y, dim=[2, 3])

    coords = torch.stack([outputs_x, outputs_y], dim=2)

    return coords


def soft_argmax(heatmaps, normalized_coordinates=True):
    if normalized_coordinates:
        values = [normalized_linspace(d, dtype=heatmaps.dtype, device=heatmaps.device)
                  for d in heatmaps.size()[2:]]
    else:
        values = [torch.arange(0, d, dtype=heatmaps.dtype, device=heatmaps.device)
                  for d in heatmaps.size()[2:]]
    coords = linear_expectation(heatmaps, values)
    # We flip the tensor like this instead of using `coords.flip(-1)` because aten::flip is not yet
    # supported by the ONNX exporter.
    coords = torch.cat(tuple(reversed(coords.split(1, -1))), -1)
    return coords


def dsnt(heatmaps, **kwargs):
    """Differentiable spatial to numerical transform.

    Args:
        heatmaps (torch.Tensor): Spatial representation of locations

    Returns:
        Numerical coordinates corresponding to the locations in the heatmaps.
    """
    return soft_argmax(heatmaps, **kwargs)


def euclidean_losses(actual, target):
    """Calculate the average Euclidean loss for multi-point samples.

    Each sample must contain `n` points, each with `d` dimensions. For example,
    in the MPII human pose estimation task n=16 (16 joint locations) and
    d=2 (locations are 2D).

    Args:
        actual (Tensor): Predictions (B x L x D)
        target (Tensor): Ground truth target (B x L x D)
    """

    assert actual.size() == target.size(), 'input tensors must have the same size'

    # Calculate Euclidean distances between actual and target locations
    diff = actual - target
    dist_sq = diff.pow(2).sum(-1, keepdim=False)
    dist = dist_sq.sqrt()
    return dist


def make_gauss(means, size, sigma, normalize=True):
    """Draw Gaussians.

    This function is differential with respect to means.

    Note on ordering: `size` expects [..., depth, height, width], whereas
    `means` expects x, y, z, ...

    Args:
        means: coordinates containing the Gaussian means (units: normalized coordinates)
        size: size of the generated images (units: pixels)
        sigma: standard deviation of the Gaussian (units: pixels)
        normalize: when set to True, the returned Gaussians will be normalized
    """

    dim_range = range(-1, -(len(size) + 1), -1)
    coords_list = [normalized_linspace(s, dtype=means.dtype, device=means.device)
                   for s in reversed(size)]

    # PDF = exp(-(x - \mu)^2 / (2 \sigma^2))

    # dists <- (x - \mu)^2
    dists = [(x - mean) ** 2 for x, mean in zip(coords_list, means.split(1, -1))]

    # ks <- -1 / (2 \sigma^2)
    stddevs = [2 * sigma / s for s in reversed(size)]
    ks = [-0.5 * (1 / stddev) ** 2 for stddev in stddevs]

    exps = [(dist * k).exp() for k, dist in zip(ks, dists)]

    # Combine dimensions of the Gaussian
    gauss = reduce(mul, [
        reduce(lambda t, d: t.unsqueeze(d), filter(lambda d: d != dim, dim_range), dist)
        for dim, dist in zip(dim_range, exps)
    ])

    if not normalize:
        return gauss

    # Normalize the Gaussians
    val_sum = reduce(lambda t, dim: t.sum(dim, keepdim=True), dim_range, gauss) + 1e-24
    return gauss / val_sum


def _divergence_reg_losses(heatmaps, mu_t, sigma_t, divergence):
    ndims = mu_t.size(-1)
    assert heatmaps.dim() == ndims + 2, 'expected heatmaps to be a {}D tensor'.format(ndims + 2)
    assert heatmaps.size()[:-ndims] == mu_t.size()[:-1]

    gauss = make_gauss(mu_t, heatmaps.size()[2:], sigma_t)
    divergences = divergence(heatmaps, gauss, ndims)
    return divergences


def _kl(p, q, ndims):
    eps = 1e-24
    unsummed_kl = p * ((p + eps).log() - (q + eps).log())
    kl_values = reduce(lambda t, _: t.sum(-1, keepdim=False), range(ndims), unsummed_kl)
    return kl_values


def _js(p, q, ndims):
    m = 0.5 * (p + q)
    return 0.5 * _kl(p, m, ndims) + 0.5 * _kl(q, m, ndims)


def js_reg_losses(heatmaps, mu_t, sigma_t):
    """Calculate Jensen-Shannon divergences between heatmaps and target Gaussians.

    Args:
        heatmaps (torch.Tensor): Heatmaps generated by the model
        mu_t (torch.Tensor): Centers of the target Gaussians (in normalized units)
        sigma_t (float): Standard deviation of the target Gaussians (in pixels)

    Returns:
        Per-location JS divergences.
    """

    return _divergence_reg_losses(heatmaps, mu_t, sigma_t, _js)


def average_loss(losses, mask=None):
    """Calculate the average of per-location losses.

    Args:
        losses (Tensor): Predictions (B x L)
        mask (Tensor, optional): Mask of points to include in the loss calculation
            (B x L), defaults to including everything
    """

    if mask is not None:
        assert mask.size() == losses.size(), 'mask must be the same size as losses'
        losses = losses * mask
        denom = mask.sum()
    else:
        denom = losses.numel()

    # Prevent division by zero
    if isinstance(denom, int):
        denom = max(denom, 1)
    else:
        denom = denom.clamp(1)

    return losses.sum() / denom


class CoordRegressionNetwork(nn.Module):
    def __init__(self, n_locations):
        super().__init__()
        self.fcn = FCN()
        self.hm_conv = nn.Conv2d(256, n_locations, kernel_size=1, bias=False)

    def forward(self, images):
        # 1. Run the images through our FCN
        fcn_out = self.fcn(images)
        # 2. Use a 1x1 conv to get one unnormalized heatmap per location
        unnormalized_heatmaps = self.hm_conv(fcn_out)

        # 3. Normalize the heatmaps
        heatmaps = flat_softmax(unnormalized_heatmaps)

        # 4. Calculate the coordinates
        coords = dsnt(heatmaps)

        return coords, heatmaps


class CoordRegressionLoss(nn.Module):
    def __init__(self):
        super().__init__()

        self.mse = torch.nn.MSELoss()

    def forward(self, heatmaps, coords, target_var, regulariser=1.0):
        euc_losses = euclidean_losses(coords, target_var)
        reg_losses = js_reg_losses(heatmaps, target_var, sigma_t=1.0)
        loss = average_loss(euc_losses + regulariser * reg_losses)
        return loss


model = CoordRegressionNetwork(1)

train_loader = torch.utils.data.DataLoader(
    train_dataset,
    batch_size=batch_size,
)
val_loader = torch.utils.data.DataLoader(
    val_dataset,
    batch_size=batch_size,
)

lr = 2e-4
optimizer = optim.RMSprop(model.parameters(), lr=2.5e-4)
criterion = CoordRegressionLoss()
device = 'cuda'
model.to(device=device)
checkpoint='./checkpoints'
os.makedirs(checkpoint, exist_ok=True)

class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def train(train_loader, model, criterion, optimizer):
    losses = AverageMeter()
    acces = AverageMeter()

    model.train()
    for i, sample in enumerate(train_loader):
        # measure data loading time

        input = sample['input_image'].to(device)
        target_var = sample['target_var_xy'].to(device)
        coords, heatmaps = model(input)

        loss = criterion(heatmaps=heatmaps, coords=coords, target_var=target_var, regulariser=0.5)

        # measure accuracy and record loss
        losses.update(loss.item(), input.size(0))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    return losses.avg, acces.avg

def validate(val_loader, model, criterion):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    acces = AverageMeter()

    # predictions

    # switch to evaluate mode
    model.eval()
    end = time.time()
    with torch.no_grad():
        for i, sample in enumerate(val_loader):
            # measure data loading time
            data_time.update(time.time() - end)

            input, heatmaps = sample['input_image'].to(device), sample['heatmap'].to(device)
            target_var = sample['target_var_xy'].to(device)
            target_weight = sample['target_weight'].to(device)

            coords, pred_heatmaps = model(input)
            score_map = pred_heatmaps.cpu()

            loss = criterion(heatmaps=heatmaps, coords=coords, target_var=target_var, regulariser=0.5)

            acc = accuracy(score_map, heatmaps.cpu(), [1])

            # measure accuracy and record loss
            losses.update(loss.item(), input.size(0))
            acces.update(acc[0], input.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

    return losses.avg, acces.avg

if __name__ == '__main__':

    for epoch in range(0, 1000):
        train_loss, train_acc = train(train_loader, model, criterion, optimizer)
        print(f'Epoch {epoch}: , Train Loss: {train_loss} , Acc: {train_acc}')

        valid_loss, valid_acc = validate(val_loader, model, criterion)

        print(f'Epoch {epoch}: , Valid Loss: {valid_loss} , Valid Accuracy: {valid_acc}')

        if epoch%10==0:
            torch.save(model.state_dict(), os.path.join(checkpoint, f'model_{epoch}.pth'))
