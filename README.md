# keypoint_detection

Keypoint Detection Algorithms.
Actually Implemented HourGlass and DSNT


to run hourglass :
python hourglass.py  --dataset mpii  -a hg --stacks 8 --blocks 1 --checkpoint checkpoint/mpii/hg8 -j 4 --dataset-path /mnt/tmc_headlight/  --annot-path label_20200615.txt
to run dstn:
python dstn.py --dataset-path /mnt/tmc_headlight/  --annot-path label_20200615.txt