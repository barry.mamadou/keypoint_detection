from torch.utils.data import Dataset
import os, csv, cv2
import imageio
from albumentations import VerticalFlip, HorizontalFlip, Rotate, Resize, Compose, KeypointParams, Equalize, \
    GaussianBlur, RandomBrightnessContrast

import numpy as np
import torch


class Generator(object):

    def __init__(self, dataset_path, annot_path, split_size=0.2, transform=None):
        img_dir = os.path.join(dataset_path, 'images')
        label_txt_path = os.path.join(dataset_path, annot_path)
        img_paths = os.listdir(img_dir)
        img_paths.sort()
        img_paths = [img_dir + '/' + p for p in img_paths]
        labels = []
        with open(label_txt_path, 'r') as f:
            reader = csv.reader(f, delimiter='\t')
            next(reader)
            for row in reader:
                labels.append([int(r) for r in row])
        self.img_paths = img_paths
        self.labels = labels
        self.transform = transform
        self.split_size = split_size

    def __call__(self):
        indices = list([ i for i in range(len(self.labels))
                         if not (np.array(self.labels[i])[-2:]).sum()])
        images_path = []
        labels = []

        for i in range(len(self.labels)):
            if i not in indices:
                images_path.append(self.img_paths[i])
                labels.append(self.labels[i])
        self.img_paths = images_path
        self.labels = labels
        split_indice = int((1 - self.split_size) * len(self.img_paths))
        train_img, train_label = self.img_paths[: split_indice], \
                                 self.labels[:split_indice]

        eval_img, eval_label = self.img_paths[split_indice:], self.labels[split_indice:]

        train_dataset = TrainDataset(train_img, train_label)
        eval_dataset = EvalDataset(eval_img, eval_label)

        return train_dataset, eval_dataset


class HeadLightDataset(Dataset):
    def __init__(self, img_paths, labels):
        self.img_paths = img_paths
        self.labels = labels
        self.size = (64, 64)

    def __getitem__(self, item):
        img_path = self.img_paths[item]
        label = np.flip(np.array(self.labels[item])[-2:], 0)
        input_img = np.array(imageio.imread(img_path))
        color_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB)

        original_keypoints = np.expand_dims(label.copy(), 0)
        original_image = color_img.copy()

        transformed = transform(color_img, label[None, ...], [Resize(height=256, width=256, p=1)])
        keypoints = np.array(transformed['keypoints'])
        image = transformed['image']
        sample = self.build_target(image, keypoints, original_keypoints)
        return sample

    def build_target(self, img, label, original_keypoints):
        transformed = transform(img, label, [Resize(height=self.size[0], width=self.size[1], p=1)])
        keypoints = np.array(transformed['keypoints'])
        image = transformed['image']
        heatmap, target_weight = gaussian_heatmap(self.size, keypoints[0])
        target_var = (keypoints * 2 + 1) / np.array(image.shape[:-1]) - 1
        return {'input_image': img, 'label': label,
                'resized_image': image, 'keypoints': keypoints,
                'heatmap': heatmap, 'target_weight': target_weight, 'target_var_yx': target_var,
                'target_var_xy': np.flip(target_var, 1),
                'original_keypoints': original_keypoints
                }

    def to_tensor(self, sample):
        for k in sample:
            sample[k] = torch.from_numpy(sample[k].copy())
            sample[k] = sample[k].to(torch.float32)
            if 'image' in k:
                sample[k] = sample[k]/ 255.0
                sample[k] = sample[k].permute((2, 0, 1))

        return sample

    def __len__(self):
        return len(self.img_paths)


class TrainDataset(HeadLightDataset):
    def __getitem__(self, item):
        sample = super().__getitem__(item)
        if np.random.random() > 0.8:
            transformed = transform(sample['input_image'], sample['label'])
            keypoints = np.array(transformed['keypoints'])
            image = transformed['image']
            sample = self.build_target(image, keypoints, sample['original_keypoints'])

        return self.to_tensor(sample)


class EvalDataset(HeadLightDataset):
    def __getitem__(self, item):
        sample = super().__getitem__(item)
        return self.to_tensor(sample)


def transform(img, keypoints, transformations=None, format='yx'):
    if transformations is None:
        transformations = [VerticalFlip(p=0.1), HorizontalFlip(p=0.1),
                           RandomBrightnessContrast(p=0.1), GaussianBlur(p=0.1), Rotate(p=0.3)]
    return Compose(transformations, p=1,
                   keypoint_params=KeypointParams(format=format))(image=img, keypoints=keypoints)


def in_range(value, range_):
    return value in range(*range_)


def gaussian_heatmap(size, points, sigma=1):
    img = np.zeros(shape=size, dtype=np.float32)
    points = np.asarray(points, dtype=np.int32)

    top, bottom, left, right, = points[0] - 3 * sigma, points[0] + 3 * sigma, points[1] - 3 * sigma, points[
        1] + 3 * sigma

    if not in_range(top, (0, size[0])) or not in_range(bottom, (0, size[0])) or not in_range(left, (0, size[1])) or \
            not in_range(right, (0, size[1])):
        return img[None, ...], np.array([[0.0]])

    y = np.linspace(top, bottom, 7, dtype=np.int32)
    x = np.linspace(left, right, 7, dtype=np.int32)
    y0 = points[0]
    x0 = points[1]

    xv, yv = np.meshgrid(x, y)
    # The gaussian is not normalized. We want the center value to be equal to 1
    gaussian = np.exp(- ((xv - x0) ** 2 + (yv - y0) ** 2) / (2 * sigma ** 2))
    img[yv, xv] = gaussian
    return (img[None, ...], np.array([[1.0]]))
